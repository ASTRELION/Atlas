import discord
import pymysql

def create_tables(cursor):
    """Creates/updates tables necessary for the bot"""

    cursor.execute(
        """
        CREATE TABLE IF NOT EXISTS guild (
            id BIGINT UNSIGNED NOT NULL PRIMARY KEY
        );
        """
    )

    cursor.execute(
        """
        CREATE TABLE IF NOT EXISTS user (
            id BIGINT UNSIGNED NOT NULL PRIMARY KEY
        );
        """
    )

    cursor.execute(
        """
        CREATE TABLE IF NOT EXISTS voice_channel (
            id BIGINT UNSIGNED NOT NULL PRIMARY KEY
        )
        """
    )

    cursor.execute(
        """
        CREATE TABLE IF NOT EXISTS text_channel (
            id BIGINT UNSIGNED NOT NULL PRIMARY KEY
        )
        """
    )

    cursor.execute(
        """
        ALTER TABLE voice_channel 
        ADD COLUMN IF NOT EXISTS guild_id BIGINT 
        ADD FOREIGN KEY (guild_id) REFERENCES guild(id);
        """
    )

    cursor.execute("SHOW TABLES")
    print(cursor.fetchall())

def drop_all(cursor):
    """Drops all tables and data"""

    cursor.execute(
        """
        DROP TABLE guild;

        DROP TABLE user;
        """
    )

def get_guild(id: int):
    """Get a guild by its ID"""
    pass

def set_guild(guild: discord.Guild):
    """Create/set guild data"""
    pass