import discord
from discord.ext import commands
import json
import datetime
import time
import typing
import re
import os
from threading import Thread
import aiohttp
import random
import math

def setup(client):
    client.add_cog(DnD(client))

class DnD(commands.Cog, name = "DnD"):
    """Dungeons and Dragons specific commands (5e)"""

    def __init__(self, client):
        self.client = client
        self.config = client.config
        self.dndAPI = "http://www.dnd5eapi.co/api/"

    def getAlphaNum(self, string: str):
        """Return string as alpha-numeric only"""
        return re.sub("[^0-9a-zA-Z]+", "", string)

    @commands.command("query")
    async def query(self, ctx, url: typing.Optional[str] = None, queryString: typing.Optional[str] = None):
        response = None
        async with aiohttp.ClientSession() as session:
            async with session.get(self.dndAPI + url) as ses:
                response = await ses.json()
                if (url != ""):
                    response = response["results"]

        await ctx.send(response)

    @commands.command("roll")
    async def roll(self, ctx, diceString: str):
        diceString = diceString.replace(" ", "")
        args = re.split("([d+-])", diceString) # split on d, +, -
        
        rolls = []
        for r in range(0, int(args[0])):
            rolls.append(random.randint(1, int(args[2])))

        rollStrings = []
        for r in range(0, len(rolls)):
            digits = int(math.log10(rolls[r])) + 1
            rollString = [
                "\u259B" + ("\u2580" * (digits + 2)) + "\u259C",
                "\u258C {} \u2590".format(rolls[r]),
                "\u2599" + ("\u2584" * (digits + 2)) + "\u259F",
            ]
            rollStrings.append(rollString)

        outputString = ""
        for line in range(0, 3):
            for r in range(0, len(rollStrings)):
                outputString += rollStrings[r][line] + " "
            outputString += "\n"

        await ctx.send("```markdown\n{}\n#Sum: {}\t#w/ Advantage: {}\t#w/ Disadvantage: {}```".format(outputString, sum(rolls), max(rolls), min(rolls)))
