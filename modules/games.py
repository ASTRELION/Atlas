import discord
from discord.ext import commands
import json
import datetime
import time
import typing
import random

class Games(commands.Cog):
    """General utility commands"""

    def __init__(self, client):
        self.client = client
        self.config = client.config    

def setup(client):
    client.add_cog(Games(client))