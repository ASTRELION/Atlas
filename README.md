# Atlas
General-purpose Discord bot made with [Discord.py](https://discordpy.readthedocs.io/en/stable/) 

## Install & Run

**Create a Discord application**

1. Navigate to https://discord.com/developers/applications
2. Click "New Application" in the top right and give it a name  
    a. *Note: by default, Atlas will change its username to the application's name*
3. Go to the "Bot" tab on the left
4. Click the "Add Bot" button
5. *Take note of the 'Token' on this page;* **DO NOT SHARE THIS TOKEN**

**Run the bot**

1. Install requirements  
    a. `Python3`  
    b. `PyMySQL`  
    c. `Discord.py`  
2. Download or clone the repository
3. Navigate into the repository directory; `cd Atlas`
4. Make a copy of the example configuration with the name `config.json`; `cp config.example.json config.json`
4. Enter the bot access token (from https://discord.com/developers/applications/your_bot_id/bot) in `config.json` where it says `your_bot_token`. Do NOT share this token with anyone (or upload to git)!
5. Run `python3 main.py`
6. *Stop the bot by pressing `ctrl-c`*

**Add the bot to your Discord server**

1. Go to the "OAuth2" tab of your Discord application (https://discord.com/developers/applications/your_bot_id/oauth2)
2. Copy your "Client ID"
3. Enter your client ID in the following URL where 'your_client_id' is, and navigate to the resulting URL: https://discord.com/oauth2/authorize?client_id=your_client_id&scope=bot
4. Depending on which *extensions* you have enabled, make sure to give the bot user sufficient permissions